#!/bin/bash

projectname=${PWD##*/}

# vscode
if [ ! -d .vscode ]; then
  cp -r drupaldocker/configs/vscode .vscode
fi 1>/dev/null

if [ ! -f "${projectname}.code-workspace" ]; then
  mv .vscode/workspace.code-workspace "${projectname}.code-workspace"
else
  rm .vscode/workspace.code-workspace
fi 1>/dev/null


if [ -d .git ]; then
  if [ -d .vscode ]; then
    git add ".vscode"
    git commit -m "chore: .vscode settings"
  fi
  if [ -f "${projectname}.code-workspace" ]; then
    git add "${projectname}.code-workspace"
    git commit -m "chore: vscode workspace"
  fi
fi
code "${projectname}.code-workspace"