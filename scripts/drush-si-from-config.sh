#!/bin/bash

if [ -f 'web/sites/default/settings.php' ]; then
  drush -yvvv si minimal --config-dir ../config/sync --locale es
else
  drush -yvvv si minimal --config-dir ../config/sync --locale es --db-url=mysql://drupal:drupal@mariadb:3306/drupal
fi
