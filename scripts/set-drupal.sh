#!/bin/bash

projectname=${PWD##*/}
profilename=minimal

if [ $1 ]; then
  profilename=$1
fi

echo $projectname

git init

echo "===> updating base packages"
docker-compose exec php composer require --no-update "drupal/core-composer-scaffold:^9" \
                                                     "drupal/core-recommended:^9" \
                                                     "drush/drush:^10" \
                                                     "wikimedia/composer-merge-plugin" \
                                                     "drupal/druparcheky_basics" \
                                                     "drupal/druparcheky_theme" \
                                                     "drupal/basic_paragraphs"

docker-compose exec php composer require --dev --no-update "druparcheky/core-dev:^9"

docker-compose exec php composer remove --no-update "drupal/core-project-message"

docker-compose exec php composer config --unset "extra.drupal-core-project-message"
docker-compose exec php composer config --unset "extra.drupal-core-project-message"

docker-compose exec php composer config "minimum-stability" dev
docker-compose exec php composer config "extra.grumphp.config-default-path" "vendor/druparcheky/core-dev/grumphp/grumphp.yml"
docker-compose exec php composer config "extra.grumphp.disable-plugin" "false"
docker-compose exec php composer config "extra.grumphp.project-path" "../../../../web"
docker-compose exec php composer config "extra.merge-plugin.include" "./vendor/druparcheky/core-dev/composer.json"

rm composer.lock

docker-compose exec php composer install


if [  ! -d 'config/sync' ]; then
  mkdir -p 'config/sync'
fi

if [  ! -d 'private/files' ]; then
  mkdir -p 'private/files'
fi

if [  ! -f 'private/salt.txt' ]; then
  salt=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)
  echo $salt > private/salt.txt
fi


if [ ! -f 'web/sites/default/settings.php' ]; then
  echo "==> creando settings.php"
  chmod 777 web/sites/default/
  cp drupaldocker/configs/drupal/settings.php web/sites/default/settings.php
fi

if [ -f 'web/sites/default/settings.php' ]; then
  echo "==> instalando drupal"
  if [ -f 'config/sync/system.site.yml' ]; then
    docker-compose exec php drush -y si ${profilename} --existing-config
  else
    docker-compose exec php drush -y si ${profilename} --locale=es --site-name ${projectname}
    docker-compose exec php drush en druparcheky_basics -y
  fi

fi

if [ -d behat_tests ]
then
  docker-compose exec php bash behat_tests/local-tests.sh       
fi

docker-compose exec php drush rq | grep Error
docker-compose exec php drush uli
